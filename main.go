package main

import (
	"log"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("pong"))
	})

	port := os.Getenv("PING_SERVER_PORT")
	if port == "" {
		port = "8080"
	}

	log.Print("Starting ping server on :", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
