package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestPing(t *testing.T) {
	port := os.Getenv("PING_SERVER_PORT")
	if port == "" {
		port = "8080"
	}

	res, err := http.Get(fmt.Sprintf("http://localhost:%s/ping", port))
	require.Nil(t, err)
	require.Equal(t, http.StatusOK, res.StatusCode)

	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	require.Nil(t, err)
	assert.Equal(t, string(data), "pong")
}
